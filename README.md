# Java Spring Boot Cat

> File Structure:

    - DemoApplication
    
    - Models
        -Eg. Student
        
    - FeignRestClient
    
    - ClientConnection
    
## DemoApplication:
It's used to run the main method
add the @EnableFeignClients annotation to it

***Code Sample***
```
@SpringBootApplication
@EnableFeignClients
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
```
    
## Models:
It's used to model the class with the database in mind.
Add the fields, private empty constructor, public constructor, getters and setters, tostring method

***Code Sample***
```
public class MockStudent {

    private Long id;
    private String studentNumber;
    private String firstName;
    private String score;

    private MockStudent(){}

    public MockStudent(String studentNumber, String firstName) {
        this.studentNumber = studentNumber;
        this.firstName = firstName;
    }
}
```

## FeignRestClient:
It's used to create an inteface to access the url and its endpoints

***Code Sample***
```
@FeignClient(name="mockclient", url="http://10.51.11.230:1000")
public interface MockFeignClient {

    @RequestMapping(method= RequestMethod.POST, value="students")
    MockStudent createStudent(@RequestBody MockStudent mockStudent);

    @RequestMapping(method = RequestMethod.GET, value = "students")
    MockStudent getById(@RequestParam(name = "studentNumber") String studentNumber);

    @RequestMapping(method = RequestMethod.GET, value = "lecturers")
    List<MockLecturer> getAllLecturers();

    @RequestMapping(method = RequestMethod.POST, value="appointments")
    MockAppointments createAppointment(@RequestBody MockAppointments mockAppointments);

    @RequestMapping(method=RequestMethod.PATCH, value="appointments/{id}")
    MockAppointments confirmAppointment(@RequestParam("studentId") Long studentId, @PathVariable(name="id") Long appointmentId);
}

```

## ClientConnection:
It's used to consume the endpoints from the FeignRestClient

***Code Sample***
```
@Component
public class MockClientConnection implements CommandLineRunner {

    private final MockFeignClient mockFeignClient;

    public MockClientConnection(MockFeignClient mockFeignClient) {
        this.mockFeignClient = mockFeignClient;
    }

    @Override
    public void run(String... args) throws Exception {

        MockStudent student = mockFeignClient.createStudent(new MockStudent("95006", "Sharon"));
        System.out.println("Student added: "+ student);

        MockStudent getStudent = mockFeignClient.getById(student.getStudentNumber());
        System.out.println("Me:" + getStudent);

        List<MockLecturer> allLecturers = mockFeignClient.getAllLecturers();
        System.out.println("All Lecturers:" +allLecturers);

        MockAppointments appointment = mockFeignClient.createAppointment(new MockAppointments(student.getid(), (long)2));
        System.out.println("Appointment added: "+ appointment);

        MockAppointments confirmAppointment = mockFeignClient.confirmAppointment(student.getid(), appointment.getId());
        System.out.println("Appointment confirmed: "+ confirmAppointment);
    }
}

```

### Sample Question:
> Models:
```
public class Student {

    private Long id;
    private String studentName;
    private String studentNumber;
    private String enrollementKey;
    private Boolean validated;
    private String score;
    
    private List<Unit> units;

    private Student(){}

    public Student(String studentNumber, String studentName) {
        this.studentNumber = studentNumber;
        this.studentName = studentName;
    }
}

public class Unit {

    private Long id;
    private String name;

    private Unit(){}

    public Unit(String name) {
        this.name = name;
    }
}
```

> @Question1: Register an account
```
@RequestMapping(method= RequestMethod.POST, value="registrations")
Student createStudent(@RequestBody Student student)

Student student = FeignRestClient.createStudent(new Student("95006", "Sharon"));
System.out.println("Student added: "+ student);
```

> @Question2: View Available units
```
@RequestMapping(method = RequestMethod.GET, value = "units")
List<Unit> getAllUnits();

List<Unit> allUnits = FeignRestClient.getAllUnits();
System.out.println("All Units:" +allUnits);
```

> @Question3: Enroll into any unit
```
@RequestMapping(method=RequestMethod.POST, value="{unitId}/enroll/{studentId}")
Student enrollUnit(@PathVariable("unitId") Long unitId, @PathVariable(name="studentId") Long studentId);

Student enrollUnit = FeignRestClient.enrollUnit((long)2, student.getid());
System.out.println("Unit Enrollement confirmed: "+ enrollUnit);
```

> @Question4: Validate unit of choice
```
@RequestMapping(method=RequestMethod.POST, value="{unitId}/validate/{enrollementKey}")
Student validateUnit(@PathVariable("unitId") Long unitId, @PathVariable(name="enrollementKey") String enrollementKey);

Student validated = FeignRestClient.validateUnit((long)2, student.getenrollementKey());
System.out.println("Unit Validation confirmed: "+ validated);
```

