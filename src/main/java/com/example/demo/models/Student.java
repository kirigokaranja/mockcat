package com.example.demo.models;

import java.util.List;

public class Student {

    private Long id;
    private String studentName;
    private String studentNumber;
    private String enrollementKey;
    private Boolean validated;
    private String score;

    private Unit unit;

    private Student() {
    }

    public Student(String studentName, String studentNumber) {
        this.studentName = studentName;
        this.studentNumber = studentNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getEnrollementKey() {
        return enrollementKey;
    }

    public void setEnrollementKey(String enrollementKey) {
        this.enrollementKey = enrollementKey;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }



    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", studentName='" + studentName + '\'' +
                ", studentNumber='" + studentNumber + '\'' +
                ", enrollementKey='" + enrollementKey + '\'' +
                ", validated=" + validated +
                ", score='" + score + '\'' +

                '}';
    }
}
