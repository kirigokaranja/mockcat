package com.example.demo;

import com.example.demo.models.Student;
import com.example.demo.models.Unit;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="client", url="http://10.55.8.28/api/", configuration = FeignConfig.class)
public interface FeignRestClient {

//    @RequestMapping(method= RequestMethod.POST, value="students")
//    MockStudent createStudent(@RequestBody MockStudent mockStudent);
//
//    @RequestMapping(method = RequestMethod.GET, value = "students")
//    MockStudent getById(@RequestParam(name = "studentNumber") String studentNumber);
//
//    @RequestMapping(method = RequestMethod.GET, value = "lecturers")
//    List<MockLecturer> getAllLecturers();
//
//    @RequestMapping(method = RequestMethod.POST, value="appointments")
//    MockAppointments createAppointment(@RequestBody MockAppointments mockAppointments);
//
//    @RequestMapping(method=RequestMethod.PATCH, value="appointments/{id}")
//    MockAppointments confirmAppointment(@RequestParam("studentId") Long studentId, @PathVariable(name="id") Long appointmentId);



    @RequestMapping(method= RequestMethod.POST, value="students")
    Student createStudent(@RequestBody Student student);

    @RequestMapping(method = RequestMethod.GET, value = "units")
    List<Unit> getAllUnits();

    @RequestMapping(method=RequestMethod.POST, value="{unitId}/enroll/{studentId}")
    Student enrollUnit(@PathVariable("unitId") Long unitId, @PathVariable(name="studentId") Long studentId);

    @RequestMapping(method=RequestMethod.POST, value="{unitId}/validate/{enrollementKey}")
    Student validateUnit(@PathVariable(name="unitId") Long unitId, @PathVariable(name="enrollementKey") String enrollementKey);




//    @RequestMapping(method=RequestMethod.POST, value="companies/{companyId}/attachments")
//    Student requestAttachment(@PathVariable("companyId") Long companyId, @RequestParam("studentId") Long studentId);
//
//    @RequestMapping(method=RequestMethod.PATCH, value="companies/{companyId}/attachments")
//    Department requestDepartment(@PathVariable("companyId") Long companyId, @RequestParam("sstudentId") Long studentId, @RequestParam("departmentId") Long departmentId);
//
//    @RequestMapping(method=RequestMethod.DELETE, value="departments")
//    Department rejectDepartment(@RequestBody Department department);
}
