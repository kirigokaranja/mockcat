package com.example.demo;

import com.example.demo.models.Student;
import com.example.demo.models.Unit;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClientConnection implements CommandLineRunner {

    private final FeignRestClient feignRestClient;

    public ClientConnection(FeignRestClient feignRestClient) {
        this.feignRestClient = feignRestClient;
    }

    @Override
    public void run(String... args) throws Exception {

        Student student = feignRestClient.createStudent(new Student("Sharon Kirigo", "95006"));
        System.out.println("Student added: "+ student);

        List<Unit> allUnits = feignRestClient.getAllUnits();
        System.out.println("All Units:" +allUnits);

        Student enrollUnit = feignRestClient.enrollUnit((long)2, (long)7);
        System.out.println("Unit Enrollement confirmed: "+ enrollUnit);

        Student validated = feignRestClient.validateUnit((long)2, student.getEnrollementKey());
        System.out.println("Unit Validation confirmed: "+ validated);

    }
}